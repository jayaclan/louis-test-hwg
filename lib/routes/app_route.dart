import 'package:get/get_navigation/get_navigation.dart';
import 'package:louis_hwg/routes/route_constant.dart';
import 'package:louis_hwg/tigalima/TigaLimaScreen.dart';

class AppRoute {
  static final all = [
    GetPage(
        name: RouteConstant.tigalima,
        page: () => const TigaLimaScreen(),
        transition: Transition.leftToRight),


  ];
}
