import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'app_colors.dart';

class TextFieldCustom extends StatelessWidget {
  final String label;
  final String hint;
  final ValueChanged<String>? onChanged;
  final TextEditingController? controller;
  final bool statusNew;
  final bool isPassword;
  late bool showPassword;
  final String? errorText;
  final List<TextInputFormatter>? inputFormatters;
  final TextCapitalization textCapitalization;
  final ValueChanged<String>? onSubmitted;

  final GestureTapCallback? onTap;
  final TextInputType? keyboardType;
  final Function? changeShowPassword;
  OutlineInputBorder newBorder = const OutlineInputBorder(
    borderSide: BorderSide(width: 1, color: AppColors.green),
    borderRadius: BorderRadius.all(Radius.circular(4.0)),
  );

  OutlineInputBorder ussualyBorder = const OutlineInputBorder(
    borderSide: BorderSide(width: 1, color: AppColors.blueSecond2),
    borderRadius: BorderRadius.all(Radius.circular(4.0)),
  );

  final int? minLines;
  final int? maxLines;
  final showClear;
  final bool? enabled;
  TextFieldCustom({
    Key? key,
    required this.label,
    required this.hint,
    required this.onChanged,
    this.controller,
    this.statusNew = false,
    this.errorText,
    this.inputFormatters,
    this.textCapitalization = TextCapitalization.none,
    this.onSubmitted,

    this.onTap,
    this.minLines,
    this.maxLines = 1,
    this.isPassword = false,
    this.showPassword = false,
    this.keyboardType,
    this.changeShowPassword,
    this.showClear = true, this.enabled,

  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextField(
      onTap: onTap,
      enabled : enabled,
      keyboardType: keyboardType,
      onTapOutside: (value){
        FocusManager.instance.primaryFocus?.unfocus();
      },
      onSubmitted: onSubmitted,
      textCapitalization: textCapitalization,
      inputFormatters: inputFormatters,
      controller: controller,
      onChanged: onChanged,
      minLines: minLines,
      obscureText: isPassword && showPassword == false,
      // Set this
      maxLines: maxLines,
      style: const TextStyle(
          color: AppColors.blueSecond2,
          fontSize: 14,
          fontWeight: FontWeight.w400),
      decoration: InputDecoration(
        suffixIcon: statusNew
            ? Icon(Icons.done)
            : isPassword
                ? Row(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [

                      IconButton(
                          iconSize: 24,
                          splashRadius: 24,
                          onPressed: () {
                            if (changeShowPassword != null) {
                              changeShowPassword!();
                            }
                          },
                          icon: showPassword
                              ? const Icon(
                            Icons.visibility_off,
                            size: 16,
                            color: AppColors.blueSecond,
                          )
                              : const Icon(
                            Icons.visibility,
                            size: 16,
                            color: AppColors.blueSecond,
                          )),
                      controller!.value.text != ""
                          ? IconButton(
                              iconSize: 24,
                              splashRadius: 24,
                              onPressed: () {
                                controller?.clear();
                              },
                              icon: const Icon(
                                Icons.close,
                                size: 16,
                                color: AppColors.blueSecond,
                              ))
                          : const SizedBox(width: 4)
                    ],
                  )
                : controller!.value.text != "" && showClear
                    ? IconButton(
                        iconSize: 24,
                        splashRadius: 24,
                        onPressed: () {
                          controller?.clear();
                        },
                        icon: const Icon(
                          Icons.close,
                          size: 16,
                        ))
                    : null,
        suffixIconColor: AppColors.green,
        errorText: errorText,
        errorStyle: TextStyle(
            color: AppColors.green, fontSize: 10, fontWeight: FontWeight.w400),
        errorBorder: statusNew ? newBorder : ussualyBorder,
        focusedErrorBorder: statusNew ? newBorder : ussualyBorder,
        filled: true,
        fillColor: AppColors.white,
        labelText: label,
        labelStyle: const TextStyle(
            color: AppColors.gray, fontSize: 14, fontWeight: FontWeight.w400),
        enabledBorder: const OutlineInputBorder(
          borderSide: BorderSide(width: 1, color: AppColors.blueSecond2),
          borderRadius: BorderRadius.all(Radius.circular(4.0)),
        ),
        border: const OutlineInputBorder(
          borderSide: BorderSide(width: 1, color: AppColors.gray),
          borderRadius: BorderRadius.all(Radius.circular(4.0)),
        ),
        focusedBorder: const OutlineInputBorder(
          borderSide: BorderSide(width: 1, color: AppColors.blueSecond2),
          borderRadius: BorderRadius.all(Radius.circular(4.0)),
        ),
        hintText: hint,
        hintStyle: const TextStyle(
            color: AppColors.gray, fontSize: 14, fontWeight: FontWeight.w400),
      ),
    );
  }
}
