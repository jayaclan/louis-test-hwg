import 'dart:ui';

class AppColors {
  static const black = Color(0xff000000);
  static const background = Color(0xffEFF3F8);
  static const blackBottomBar = Color(0xff202123);
  static const white = Color(0xffffffff);
  static const white2 = Color(0xffF8FAFC);
  static const whiteLabel = Color.fromRGBO(255, 255, 255, 0.6);
  static const whiteOutline = Color.fromRGBO(255, 255, 255, 0.12);
  static const lowBackground = Color.fromRGBO(37, 39, 42, 0.7);
  static const textBlack = Color(0xff0D1C2E);
  static const blue = Color(0xff2456AD);

  static const blueSecond = Color(0xff0D1C2E);
  static const blueSecond2 = Color(0xff082A66);
  static const gray = Color(0xff667085);
  static const gray2 = Color(0xffD9D9D9);
  static const green = Color(0xff117680);
  static const yellow = Color(0xffFFC605);
  static const text = Color(0xff272639);
  static const red = Color(0xffD6222A);





  static const textgray = Color(0xffEAECF0);




}