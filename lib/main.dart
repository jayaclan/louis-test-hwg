import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:louis_hwg/routes/app_route.dart';
import 'package:louis_hwg/routes/route_constant.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Louis HWG',
      // theme: ,
      getPages: AppRoute.all,
      initialRoute: RouteConstant.tigalima,
      debugShowCheckedModeBanner: false,
      defaultTransition: Platform.isIOS ? Transition.native : Transition.zoom,
      transitionDuration:
          Platform.isIOS ? null : const Duration(milliseconds: 300),
    );
  }
}
