import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:louis_hwg/util/app_colors.dart';
import 'package:url_launcher/url_launcher.dart';

import '../util/textFieldCustom.dart';
import 'TigaLimaController.dart';

class TigaLimaScreen extends StatelessWidget {
  const TigaLimaScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<TigaLimaController>(
      init: TigaLimaController(),
      builder: (TigaLimaController controller) {
        return Scaffold(
            body: SafeArea(
          child: Container(
              padding:
                  const EdgeInsets.symmetric(horizontal: 15.0, vertical: 12),
              width: double.infinity,
              height: double.infinity,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    "Pengerja : Louis Rudy Valen",
                    textAlign: TextAlign.start,
                    style: TextStyle(
                        color: AppColors.text,
                        fontSize: 14,
                        fontWeight: FontWeight.w400),
                  ),
                  GestureDetector(
                    onTap: () async {
                      final _url = Uri.parse("https://www.linkedin.com/in/louisvalennn/");
                      if (!await launchUrl(_url,
                          mode: LaunchMode.externalApplication)) {
                        // <--
                        throw Exception('Could not launch $_url');
                      }
                    },
                    child: const Text(
                      "LinkedIn (Tap for info)",
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          color: AppColors.blueSecond2,
                          fontSize: 16,
                          fontWeight: FontWeight.w700),
                    ),
                  ),
                  SizedBox(height: 4,),
                  const Text(
                    "Soal :\nAngka habis bagi 3 Print 'Tiga'\n"
                    "Angka habis bagi 5 Print 'Lima'\n"
                    "Angka habis bagi 3 dan 5 Print 'TigaLima'\n"
                    "Jika tidak habis di bagi 3 atau 5 print angka tersebut'\n",
                    textAlign: TextAlign.start,
                    style: TextStyle(
                        color: AppColors.blackBottomBar,
                        fontSize: 14,
                        fontWeight: FontWeight.w400),
                  ),
                  TextFieldCustom(
                    controller: controller.numberEditable,
                    label: "Masukan Nomor",
                    hint: "Isi Berapa Pun, Nomor otomatis generate",
                    onChanged: (value) {
                      controller.setNumber(int.tryParse(value) ?? 0);
                    },
                    keyboardType: TextInputType.number,
                    inputFormatters: <TextInputFormatter>[
                      FilteringTextInputFormatter.digitsOnly,
                    ],
                    showClear: false,

                  ),
                  Expanded(
                      flex: 1,
                      child: ListView.builder(
                          shrinkWrap: true,
                          itemCount: controller.listNumber.length,
                          itemBuilder: (context, index) {
                            return Text(
                              controller.listNumber[index].number,
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                  color:
                                      controller.listNumber[index].colorNumber,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400),
                            );
                          }))
                ],
              )),
        ));
      },
    );
  }
}
