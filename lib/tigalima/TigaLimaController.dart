import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:louis_hwg/util/app_colors.dart';

import 'model/TigaLimaModel.dart';

class TigaLimaController extends GetxController {
  late TextEditingController numberEditable = TextEditingController();

  List<TigaLimaModel> listNumber = [];

  @override
  void onInit() async {
    super.onInit();
  }

  void setNumber(int lastRange) {
    listNumber.clear();

    for (int i = 1; i <= lastRange; i++) {
      if (i % 3 == 0 && i % 5 == 0) {
        listNumber.add(TigaLimaModel("TigaLima ($i)", AppColors.red));
      } else if (i % 3 == 0) {
        listNumber.add(TigaLimaModel("Tiga ($i)", AppColors.green));
      } else if (i % 5 == 0) {
        listNumber.add(TigaLimaModel("Lima ($i)", AppColors.yellow));
      } else {
        listNumber.add(TigaLimaModel("$i", AppColors.blueSecond2));
      }
    }
    update();
  }
}
