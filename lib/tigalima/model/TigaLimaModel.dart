
import 'dart:ui';

class TigaLimaModel {
  final String number;
  final Color colorNumber;

  TigaLimaModel(this.number, this.colorNumber);
}